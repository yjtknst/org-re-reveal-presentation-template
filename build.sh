#!/bin/sh
export EMACS="/Applications/Emacs.app/Contents/MacOS/Emacs"

$EMACS --batch --no-init-file \
       --eval "(load-file \"./setup.el\")" \
       --eval "(load-file \"./multiplex-secret.el\")" \
       --eval "(org-publish \"site-local\" t)" \
       --eval "(message \"Build complete!\")"

